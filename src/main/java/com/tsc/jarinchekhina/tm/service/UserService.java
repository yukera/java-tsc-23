package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IUserRepository;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.empty.*;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.exception.user.EmailRegisteredException;
import com.tsc.jarinchekhina.tm.exception.user.LoginExistsException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        if (findByLogin(login).isPresent()) throw new LoginExistsException(login);
        if (findByEmail(email).isPresent()) throw new EmailRegisteredException(email);
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        if (findByLogin(login).isPresent()) throw new LoginExistsException(login);
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> findByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> findByEmail(@Nullable final String email) {
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        return userRepository.findByLogin(email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> removeByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> lockByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final Optional<User> user = findByLogin(login);
        if (!user.isPresent()) throw new AccessDeniedException();
        user.get().setLocked(true);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<User> unlockByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final Optional<User> user = findByLogin(login);
        if (!user.isPresent()) throw new AccessDeniedException();
        user.get().setLocked(false);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new AccessDeniedException();
        @NotNull final String hash = HashUtil.salt(password);
        user.ifPresent(e -> e.setPasswordHash(hash));
        return user.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        @NotNull final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new AccessDeniedException();
        user.ifPresent(e -> e.setFirstName(firstName));
        user.ifPresent(e -> e.setLastName(lastName));
        user.ifPresent(e -> e.setMiddleName(middleName));
        return user.get();
    }

}
