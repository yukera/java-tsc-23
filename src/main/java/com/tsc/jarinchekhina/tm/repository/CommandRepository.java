package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@NoArgsConstructor
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    @NotNull
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        for (@NotNull final AbstractCommand command : commands.values()) {
            @NotNull final String name = command.name();
            if (DataUtil.isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    @NotNull
    public Collection<String> getCommandArgs() {
        @NotNull final List<String> result = new ArrayList<>();
        for (@NotNull final AbstractCommand command : arguments.values()) {
            @NotNull final String name = command.name();
            if (DataUtil.isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@NotNull final String name) {
        return arguments.get(name);
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (!DataUtil.isEmpty(arg)) arguments.put(arg, command);
        if (!DataUtil.isEmpty(name)) commands.put(name, command);
    }

}
